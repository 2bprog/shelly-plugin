# CHANGELOG
## v0.3.1
- update documentation
## v0.3.0
- add [Shelly Plug / PlugS](https://shelly-api-docs.shelly.cloud/#shelly-plug-plugs)
- add documentation for [status feedback](documentation/status_feedback.md)
## v0.2.0
- add [Shelly Dimmer/SL](https://shelly-api-docs.shelly.cloud/#shelly-dimmer-sl)
## v0.1.1
- refactoring Shelly Relay: merge HTTP Control and HTTP Sensor
## v0.1.0
- add [Shelly H&T](https://shelly-api-docs.shelly.cloud/#shelly-h-amp-t)
- add [Shelly Smoke](https://shelly-api-docs.shelly.cloud/#shelly-smoke)
- refactoring Shelly Relay (shelly 1/1PM/2/2-5/4Pro)
- update README
## v0.0.2
- add 1 HTTP captor to show total consumption
- update README
## v0.0.1
- add 3 devices for each channel
- add 2 devices for each channel
- add LICENCE
- add README
- init project