Pour avoir le retour d'état il faut:
- aller sur votre interface [Eedomus](https://secure.eedomus.com)
- avoir le [Mode Expert](https://doc.eedomus.com/view/Mode_Expert) d'actif
- aller sur le périphérique qui affiche l'état et ouvrir les __Paramètres Expert__
![Param Expert](screenshot/shelly_expert.png "Mode expert du périphérique") et cliqué sur la __clé bleu__
- une nouvelle page s'affiche et choisissez :
![Status Feedback](screenshot/shelly_status_feedback.png "Url pour le retour d'état")
    - Destination -> Local
    - Direction -> SET
    - Valeur -> Off
    - __Copier l'URL__ et mettre à la place de __[url_parametre_expert_OFF]__ de __VAR3__
    - Valeur -> On (Celle qui met 1 en value)
    - __Copier l'URL__ et mettre à la place de __[url_parametre_expert_ON]__ de __VAR3__

Pour activer le retour d'état c'est le module Shelly qui va envoyer la valeur vers votre Eedomus.

On va paramétrer le module en lançant les 2 URLS en les copier/coller dans un onglet de votre navigateur. Si tous se passe bien un JSON doit s'afficher en retour.
