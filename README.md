# shelly-plugin

## TL;DR
Permet de commander les modules de type *relais*[^1], H&T, Smoke, Dimmer et Plug S [Shelly](https://shelly.cloud/) avec une [Eedomus](https://www.eedomus.com/).
[^1]: relais compatibles : [shelly1](https://shelly.cloud/product/shelly-1-open-source/)/[shelly1pm](https://shelly.cloud/product/wifi-smart-home-automation-shelly-1pm-switch-relay/)/shelly2/[shelly2-5](https://shelly.cloud/product/wifi-smart-home-automation-shelly-25-switch-relay-roller-shutter/)/[shelly4pro](https://shelly.cloud/product/shelly-4pro/)

## Détails
Dans le cas du choix d'un  __**Shelly Relay**__, ce plugin permet de créer 4 canaux:
** Pensez Ã  supprimer les canaux non utilisés ! **
- 3 [périphériques](https://doc.eedomus.com/view/Ajouter_un_p%C3%A9riph%C3%A9rique) :
  - 1 périphérique [Actionneur HTTP](https://doc.eedomus.com/view/Actionneurs_HTTP) pour __commander__ un canal (On/Off) avec synchronisation de l'__état__.
      Il est est rafraichi toute les minutes.
  - 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre la __consommation__ instantané en Watt du canal.
      Il est est rafraichi toute les minutes et MAJ aussi via une régle lorsque l'on change la valeur du canal depuis l'actionneur HTTP.
  - 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre la __consommation__ totale en Watt du canal.
  Il est est rafraichi toute les minutes et MAJ aussi via une régle lorsque l'on change la valeur du canal depuis l'actionneur HTTP.
- 1 [règle](https://doc.eedomus.com/view/Moteur_de_r%C3%A8gles) :
  - Shelly - MAJ Channel {idChannel} :
    - lorsque l'actionneur HTTP change de valeur ->
      - MAJ de la puissance intantané.
      - MAJ de la consommation totale.

Pour le  __**Shelly H&T**__:
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre la __température__ en °C du Shelly H&T.
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre l' __humidité__ en % du Shelly H&T.
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre le __niveau de batterie__ en % du Shelly H&T.

Pour le  __**Shelly Smoke**__:
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre l' __état__ du Shelly Smoke OK/Alarme.
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre l' __température__ en °C du Shelly Smoke.
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre le __niveau de batterie__ en % du Shelly Smoke.

Pour le  __**Shelly Dimmer / SL**__:
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour __commander__ la lumière (brightness) avec synchroisation de l'__état__.
  Il est est rafraichi toute les minutes.

Pour le  __**Shelly Plug / Plug S**__:
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour __commander__ la prise (On/Off) avec synchronisation de l'__état__.
  Il est est rafraichi toute les minutes.
  [Documentation retour d'état](documentation/status_feedback.md) CF __[VAR2] et [VAR3]__.
 - 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre la __consommation__ instantané en Watt du canal.
      Il est est rafraichi toute les minutes et MAJ aussi via une régle lorsque l'on change la valeur du canal depuis l'actionneur HTTP.
  - 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre la __consommation__ totale en Watt du canal.
  Il est est rafraichi toute les minutes et MAJ aussi via une régle lorsque l'on change la valeur du canal depuis l'actionneur HTTP.
- 1 [règle](https://doc.eedomus.com/view/Moteur_de_r%C3%A8gles) :
  - Shelly - MAJ Plug :
    - lorsque l'actionneur HTTP change de valeur ->
      - MAJ de la puissance intantané.
      - MAJ de la consommation totale.

## Documentation
- Création d'un [plugin Eedomus](https://doc.eedomus.com/view/Le_store_eedomus)
- [Programmation des plugins Eedomus](https://docs.google.com/document/d/1j6Nd5LZ5Dou5g8pzK2zuFkeo3exunZxRvIQN2ZUaZ0s) de @Merguez07
- [API Shelly](https://shelly-api-docs.shelly.cloud/)

## Code Source
https://gitlab.com/eedomus/shelly-plugin


Enjoy (^_^)
